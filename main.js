/** Rain Game with classes **/

window.onload = myGame;
enchant();

function myGame() {
    // New game with a 320x320 pixel canavs, 24 frames/sec.
    var game = new Core(320, 320);
    game.fps = 24;

    // Preload assets.
    game.preload('droplet24.png');
    game.preload('usertrash32.png');
    game.preload('sky.png');

    // Additional game properties (accessible to all helper functions).
    game.caughtDroplets = 0; // How many droplets caught?

    // This object starts counting down once the game is started.
    var timerDown = {
        frameCount: 10 * game.fps, // total needed frames.
        tick: function () { // call tick() every frame.
            if (game.isStarted) {
                this.frameCount -= 1; // count down instead of up.
            }
        }
    };

    // Specify what should happen when the game loads.
    game.onload = function () {
        // Background image.
        var bg = new Sprite(game.width, game.height);
        bg.image = game.assets['sky.png'];
        game.rootScene.addChild(bg);

        // Create a label to show time. (Placed at 0,0 by default.)
        var timeLabel = new Label("Collect droplets as fast as you can.");
        game.rootScene.addChild(timeLabel);

        // Make the collector
        var collector = new Collector();

        // Add an event listener for each frame.
        game.addEventListener(Event.ENTER_FRAME, function () {
            if (timerDown.frameCount > 0) { // if the game is on ...
                // Make a droplet every once in a while.
                if (game.frame % 24 === 0) {
                    // Just make a droplet, no need to have a reference to it.
                    new Droplet(collector);
                }

                // Tick the timer.
                timerDown.tick();

                // Update label.
                timeLabel.text = 'Time remaining: ' +
                    Math.ceil(timerDown.frameCount / game.fps);
            } else if (timerDown.frameCount !== -1) { // Game over?
                // indicate that the "game over" by setting frameCount to -1
                timerDown.frameCount = -1;
                // add final score to screen
                timeLabel.text += '<br>Droplets: ' + game.caughtDroplets;
                // end the game
                game.end();
            }
        });
    };

    // Start the game.
    game.start();

    // =================================================================
    // ==== Begin game class definitions ===============================
    // =================================================================
    /** A Droplet is Sprite that falls from above the scene and can be
     * caught by the collector. If the Droplet collides with the collector,
     * remove the Droplet and give player a point. If the Droplet goes off
     * screen, remove it. */
    var Droplet = Class.create(Sprite, { // Create a class inheriting Sprite.
        initialize: function (collector) { // Called automatically.
            Sprite.call(this, 24, 24); // Applies Sprite's initializations.
            this.image = game.assets['droplet24.png'];

            // Add a gravity property.
            this.gravity = 5; // How fast will the droplet drop?

            // Position droplet at random x and random y above screen.
            this.x = randomInt(0, game.width - this.width);
            this.y = randomInt(-1 * game.height, -1 * this.height);

            // Add droplet to scene.
            game.rootScene.addChild(this);

            // Add an event listener to the droplet to move it.
            this.addEventListener(Event.ENTER_FRAME, function () {
                // Game starts when first droplet starts falling.
                game.isStarted = true;

                // Move.
                if (this.y > game.height) { // if below canvas, remove.
                    game.rootScene.removeChild(this);
                } else { // else move down as usual.
                    this.y += this.gravity;
                }

                // Check for collision.
                if (this.intersect(collector)) {
                    game.rootScene.removeChild(this);
                    catchDroplet();
                }
            });
        }
    }); // End of Droplet definition


    /** A Collector is Sprite that can be moved around the bottom of the
     * scene. */
    var Collector = Class.create(Sprite, { // Create a class inheriting Sprite.
        initialize: function () { // Called automatically.
            Sprite.call(this, 32, 32); // Applies Sprite's initializations.
            this.image = game.assets['usertrash32.png'];

            // Add a speed property.
            this.speed = 4; // How fast will collector move?

            this.x = (game.width + this.width) / 2;
            this.y = game.height - this.height;

            // Add collector to scene.
            game.rootScene.addChild(this);

            // Event listeners for collector.
            this.addEventListener(Event.ENTER_FRAME, function () {
                // Move.
                if (game.input.right && !game.input.left) {
                    this.x += this.speed;
                } else if (game.input.left && !game.input.right) {
                    this.x -= this.speed;
                }

                // Check limits.
                if (this.x > game.width - this.width) {
                    this.x = game.width - this.width;
                } else if (this.x < 0) {
                    this.x = 0;
                }
            });
        }
    }); // End of Collector definition
    // =================================================================
    // ==== End game class definitions =================================
    // =================================================================

    // =================================================================
    // ==== Begin game helper functions ================================
    // =================================================================
    /** Give the player a point for catching a drop. */
    function catchDroplet() {
        game.caughtDroplets += 1;
    }

    /** Generate a random integer between low and high (inclusive). */
    function randomInt(low, high) {
        return low + Math.floor((high + 1 - low) * Math.random());
    }
    // =================================================================
    // ==== End game helper functions ==================================
    // =================================================================
}
